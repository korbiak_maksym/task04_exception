package com.korbiak.model;

import java.util.ArrayList;

public interface Model {
    ArrayList<Integer> getOdd();
    ArrayList<Integer> getEven();
    int getSumOfOdd();
    int getSumOfEven();
}
