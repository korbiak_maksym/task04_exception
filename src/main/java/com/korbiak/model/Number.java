package com.korbiak.model;

import java.util.ArrayList;

public class Number implements AutoCloseable {
    private ArrayList<Integer> odd;
    private ArrayList<Integer> even;

    public Number(final int start, final int end) {
        odd = new ArrayList<Integer>();
        even = new ArrayList<Integer>();
        for (int i = start; i <= end; i++) {
            if (i % 2 != 0) {
                odd.add(i);
            } else {
                even.add(i);
            }
        }
    }

    public final int sumOfOdd() {
        int sum = 0;
        for (int i = 0; i < odd.size(); i++) {
            sum += odd.get(i);
        }
        return sum;
    }

    public final int sumOfEven() {
        int sum = 0;
        for (int i = 0; i < even.size(); i++) {
            sum += even.get(i);
        }
        return sum;
    }

    public final ArrayList<Integer> getOdd() {
        return odd;
    }

    public final void setOdd(final ArrayList<Integer> odd) {
        this.odd = odd;
    }

    public final ArrayList<Integer> getEven() {
        return even;
    }

    public final void setEven(final ArrayList<Integer> even) {
        this.even = even;
    }

    @Override
    public void close() throws Exception {
        throw new Exception();
    }
}
