package com.korbiak.model;

import java.util.ArrayList;

public class BusinessLogic implements Model {
    private Number number;

    public BusinessLogic(int start, int end) {
        try (Number number = new Number(start, end)) {
            if (start >= end) {
                throw new Misconduct();
            }
            this.number = number;
        } catch (Misconduct misconduct) {
            System.out.println("Misconduct ex");
            start = 0;
            end = 1;
            this.number = new Number(start, end);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public final ArrayList<Integer> getOdd() {
        return number.getOdd();
    }

    public final ArrayList<Integer> getEven() {
        return number.getEven();
    }

    public final int getSumOfOdd() {
        if (number.sumOfOdd() == 0) {
            new ErrorOfSum();
        }
        return number.sumOfOdd();
    }

    public final int getSumOfEven() {
        if (number.sumOfEven() == 0) {
            new ErrorOfSum();
        }
        return number.sumOfEven();
    }
}
