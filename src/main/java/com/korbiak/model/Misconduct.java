package com.korbiak.model;

public class Misconduct extends Exception {
    @Override
    public final String getMessage() {
        return "Misconduct";
    }
}
