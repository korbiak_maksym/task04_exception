package com.korbiak;

import com.korbiak.view.View;

public class Application {
    private Application() {
    }

    public static void main(final String[] args) {
        new View().show();
    }
}
