package com.korbiak.controller;

import java.util.ArrayList;

public interface Controller {
    ArrayList<Integer> getOdd();
    ArrayList<Integer> getEven();
    int getSumOfOdd();
    int getSumOfEven();
}
