package com.korbiak.controller;

import com.korbiak.model.BusinessLogic;

import java.util.ArrayList;

public class ControllerImpl implements Controller {
    final BusinessLogic businessLogic;

    public ControllerImpl(final int start, final int end) {
        this.businessLogic = new BusinessLogic(start, end);
    }

    public final ArrayList<Integer> getOdd() {
        return businessLogic.getOdd();
    }

    public final ArrayList<Integer> getEven() {
        return businessLogic.getEven();
    }

    public final int getSumOfOdd() {
        return businessLogic.getSumOfOdd();
    }

    public final int getSumOfEven() {
        return businessLogic.getSumOfEven();
    }
}
