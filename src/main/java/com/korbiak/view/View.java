package com.korbiak.view;

import com.korbiak.controller.Controller;
import com.korbiak.controller.ControllerImpl;

import java.util.ArrayList;
import java.util.Scanner;

public class View {
    private Scanner scanner;
    private Controller controller;

    public View() {
        scanner = new Scanner(System.in);
        controller = new ControllerImpl(0, 1);
    }

    public final void show() {
        int k = 0;
        do {
            System.out.println("-------------------------------------");
            System.out.println("1- Build numbers");
            System.out.println("2- Print odd number");
            System.out.println("3- Print even numbers");
            System.out.println("4- Print sum of odd");
            System.out.println("5- Print sum of even");
            System.out.println("6- Exit");
            System.out.println("Your answer:");
            int answer = scanner.nextInt();
            if (answer == 1) {
                pressButton1();
            } else if (answer == 2) {
                pressButton2();
            } else if (answer == 3) {
                pressButton3();
            } else if (answer == 4) {
                pressButton4();
            } else if (answer == 5) {
                pressButton5();
            } else if (answer == 6) {
                k = 1;
            }
        } while (k == 0);
    }

    private void pressButton1() {
        System.out.println("-------------------------------------------------");
        int start = 0;
        int end = 0;
        System.out.println("Set start:");
        start = scanner.nextInt();
        System.out.println("Set end:");
        end = scanner.nextInt();
        controller = new ControllerImpl(start, end);
    }

    private void pressButton2() {
        System.out.println("-------------------------------------------------");
        System.out.println("Odd numbers:");
        ArrayList<Integer> list = controller.getOdd();
        for (int i = 0; i < list.size(); i++) {
            System.out.print(list.get(i) + " ");
        }
        System.out.println();
    }

    private void pressButton3() {
        System.out.println("-------------------------------------------------");
        System.out.println("Even numbers:");
        ArrayList<Integer> list = controller.getEven();
        for (int i = 0; i < list.size(); i++) {
            System.out.print(list.get(i) + " ");
        }
        System.out.println();
    }

    private void pressButton4() {
        System.out.println("Sum of odd:" + controller.getSumOfOdd());
    }

    private void pressButton5() {
        System.out.println("Sum of even:" + controller.getSumOfEven());
    }

}
